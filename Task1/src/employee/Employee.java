package employee;

public class Employee {
    private String fistName;
    private String lastName;
    private String job;
    private int salary;

    public Employee(String fistName, String lastName, String job, int salary) {
        this.fistName = fistName;
        this.lastName = lastName;
        this.job = job;
        this.salary = salary;
    }

    public String getFistName() {
        return fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getJob() {
        return job;
    }

    public int getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "Fist name='" + fistName + '\'' +
                ", Last name='" + lastName + '\'' +
                ", Job='" + job + '\'' +
                ", Salary=" + salary +
                '}';
    }
}
