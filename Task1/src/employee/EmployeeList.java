package employee;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class EmployeeList {
    private List<Employee> employeeList;
    private static final int employeeInfoCount = 4;
    public void setEmployeeList(){
        employeeList = new ArrayList<>();
        Scanner scanUserInput = new Scanner(System.in);
        System.out.println("Input: \'First name, Last name, Job, Salary\' with \',\' as delimiter");
        System.out.println("Type \'exit\' to stop");
        String userInput;
        while (!(userInput = scanUserInput.next()).equals("exit")){
            String[] userInputParts = userInput.split(",");
            if(userInputParts.length < employeeInfoCount){
                throw new IllegalArgumentException("Not enough info about employee");
            }
            int empFieldNum = 0;
            Employee newEmployee = new Employee(userInputParts[empFieldNum++],userInputParts[empFieldNum++],userInputParts[empFieldNum++],Integer.parseInt(userInputParts[empFieldNum]));
            employeeList.add(newEmployee);
        }
    }

    public void printInfo(){
        if(employeeList == null || employeeList.isEmpty()){
            System.out.println("There are no employees in the list");
            return;
        }

        for(int i = 0; i < employeeList.size(); i++){
            System.out.println(employeeList.get(i));
        }
    }

    //Returns null if no employees with such job
    public List<Employee> getEmployeeWithJob(String job){
        if(employeeList == null){
            return null;
        }
        List<Employee> employeeWithJob = new ArrayList<>();
        for(int i = 0; i < employeeList.size(); i++){
            Employee e = employeeList.get(i);
            if(e.getJob().equals(job)){
                employeeWithJob.add(e);
            }
        }
        if(employeeWithJob.size() > 0){
            return employeeWithJob;
        } else return null;
    }

    public void sortBySalary(){
        Comparator<Employee> salaryComparator = new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return Integer.compare(o1.getSalary(),o2.getSalary());
            }
        };
        employeeList.sort(salaryComparator);
    }

    public void sortByLastName(){
        Comparator<Employee> lastNameComparator = new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o1.getLastName().compareTo(o2.getLastName());
            }
        };
        employeeList.sort(lastNameComparator);
    }

}