package figures;

public class Circle extends Figure {

    private double radius;

    public Circle(Point center, double radius){
        points = new Point[]{center};
        this.radius = radius;
    }

    @Override
    public void print() {
        System.out.println("center: " + getCenter() + " radius = " + radius);
    }

    @Override
    public void scale(double k) {
        radius = radius*k;
    }

    @Override
    public double area() {
        return Math.PI*Math.pow(radius,2);
    }

    public boolean checkPointInside(Point p){
        final double dist = getDistanceFromCenter(p);
        return dist < radius;
    }

    public boolean checkCircleInside(Circle c){
        final double dist = getDistanceFromCenter(c.getCenter());
        return dist + c.getRadius() <= radius;
    }

    public Point getCenter(){
        return points[0];
    }

    public double getRadius() {
        return radius;
    }

    private double getDistanceFromCenter(Point p){
        return (getCenter().getX() - p.getX())*(getCenter().getX() - p.getX())
                + (getCenter().getY() - p.getY())*(getCenter().getY() - p.getY());
    }
}
