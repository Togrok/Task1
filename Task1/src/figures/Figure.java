package figures;


public abstract class Figure implements FigureInterface {
    protected Point[] points;

    @Override
    public void move(double deltaX, double deltaY) {
        for(int i = 0; i < points.length; i++){
            points[i] = new Point(points[i].getX() + deltaX, points[i].getY() + deltaY);
        }
    }
}
