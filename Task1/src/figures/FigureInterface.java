package figures;

public interface FigureInterface {
    void print();
    void move(double deltaX, double deltaY);
    void scale(double k);
    double area();
}
