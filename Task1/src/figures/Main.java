package figures;

public class Main {
    public static void main(String[] args) {
        Figure circle = new Circle(new Point(2,3),1);
        circle.print();
        System.out.println(circle.area());
        System.out.println(((Circle)circle).checkPointInside(new Point(1,3.0000000000001)));
        System.out.println(((Circle)circle).checkCircleInside(new Circle(new Point(1.5,3),0.6 )));

        Figure rect = new Rectangle(new Point(10,9), new Point(4,3));
        System.out.println(rect.area());

        Figure trian = new Triangle(new Point(0,0), new Point(10,0), new Point(0,5));
        System.out.println(trian.area());
    }
}
