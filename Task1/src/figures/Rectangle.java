package figures;

public class Rectangle extends Figure {

    //set main diagonal points
    public Rectangle(Point diag1, Point diag2){
        //counter-clockwise order of points; Point_0 - Point_2 always main diagonal
        points = new Point[]{diag1,new Point(diag2.getX(),diag1.getY()),diag2,new Point(diag1.getX(),diag2.getY())};
    }

    @Override
    public void print() {
        for(int i = 0; i < points.length; i++){
            System.out.print(points[i]);
        }
    }

    @Override
    public void scale(double k) {
        for(int i = 0; i < points.length; i++){
            points[i] = new Point(k*points[i].getX(),k*points[i].getY());
        }
    }

    @Override
    public double area() {
        final Point diag1 = points[0];
        final Point diag2 = points[0];
        return Math.abs((diag1.getX() - diag2.getX())*(diag1.getY() - diag2.getY()));
    }
}
