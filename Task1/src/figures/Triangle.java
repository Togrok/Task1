package figures;

public class Triangle extends Figure {

    public Triangle(Point p1, Point p2, Point p3) {
        points = new Point[]{p1,p2,p3};
    }

    @Override
    public void print() {
        for(int i = 0; i < points.length; i++){
            System.out.print(points[i]);
        }
    }

    @Override
    public void scale(double k) {
        for(int i = 0; i < points.length; i++){
            points[i] = new Point(k*points[i].getX(),k*points[i].getY());
        }
    }

    @Override
    public double area() {
        final Point vector1 = new Point(points[1].getX() - points[0].getX(),points[1].getY() - points[0].getY());
        final Point vector2 = new Point(points[2].getX() - points[0].getX(),points[2].getY() - points[0].getY());
        //S = 0.5*|[a,b]|
        return 0.5*Math.abs(vector1.getX()*vector2.getY() - vector1.getY()*vector2.getX());
    }
}
